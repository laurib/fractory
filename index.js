import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'styled-components';
import theme, { GlobalStyle } from 'Theme';
import AuthProvider from 'Auth';
import App from './src/app';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <AuthProvider>
      <GlobalStyle />
      <App />
    </AuthProvider>
  </ThemeProvider>,
  document.querySelector('.root__element')
);
