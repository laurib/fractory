import React, { lazy, Suspense } from 'react';
import { useAuth } from 'Auth';

const LoginView = lazy(() => import('LoginView'));
const PipelineView = lazy(() => import('PipelineView'));

const App = () => {
  const { user } = useAuth();
  return <Suspense fallback={<></>}>{user ? <PipelineView /> : <LoginView />}</Suspense>;
};

export default App;
