import styled from 'styled-components';

const Icon = styled.svg.attrs(({ viewBox }) => ({
  'aria-hidden': true,
  focusable: false,
  viewBox: viewBox || '0 0 24 24',
  role: 'presentation',
}))`
  user-select: none;
  width: 1em;
  height: 1em;
  display: inline-flex;
  fill: currentColor;
  flex-shrink: 0;
  transition: all 0.2s;
  font-size: ${p => (p.size ? p.size : 'inherit')};
`;

export default Icon;
