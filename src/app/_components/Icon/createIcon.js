import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

export default function createSvgIcon(path, vb) {
  const Component = React.memo(
    React.forwardRef((props, ref) => {
      const { viewBox: viewBoxProps, ...other } = props;
      const viewBox = viewBoxProps || vb;
      return (
        <Icon ref={ref} {...(!!viewBox && { viewBox })} {...other}>
          {path}
        </Icon>
      );
    })
  );

  Component.defaultProps = {
    viewBox: undefined,
  };

  Component.propTypes = {
    viewBox: PropTypes.string,
  };

  return Component;
}
