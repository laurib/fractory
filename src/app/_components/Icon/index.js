export default from './Icon';
export { default as IconInvoice } from './icons/Invoice';
export { default as IconEuro } from './icons/Euro';
export { default as IconDelivery } from './icons/Delivery';
export { default as IconAccept } from './icons/Accept';
export { default as IconRefuse } from './icons/Refuse';
export { default as IconForward } from './icons/Forward';
export { default as IconComplete } from './icons/Complete';
