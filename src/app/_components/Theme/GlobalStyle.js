import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

  * {
    box-sizing: border-box;
  }
  html,
  body {
    background: ${({ theme: t }) => t?.palette?.body};
    color: ${({ theme: t }) => t?.palette?.main};
    margin: 0;
    padding: 0;
    text-align: left;
    text-size-adjust: none;
  }
  
  html {
    font-weight: ${({ theme: t }) => t?.typography?.fontWeightNormal};
    font-size: ${({ theme: t }) => t?.typography?.fontSizeHtml};
    line-height: ${({ theme: t }) => t?.typography?.lineHeight};
    font-family: ${({ theme: t }) => t?.typography?.fontFamilyMain};
  }

  body {
    font-size: ${({ theme: t }) => t?.typography?.fontSize};
  }

  img {
    border: none;
  }

  a {
    text-decoration: none;
    color: ${({ theme: t }) => t?.palette?.main};
  }

  ul,
  ol,
  li,
  h1,
  h2,
  h3,
  p,
  form {
    padding: 0;
    margin: 0;
  }

  h1,
  h2,
  h3 {
    font-weight: ${({ theme: t }) => t?.typography?.fontWeightBold};
    font-size: ${({ theme: t }) => t?.typography?.fontSize};
    color: ${({ theme: t }) => t?.palette?.light};
  }

  ul,
  ol {
    list-style-type: none;
  }

  b,
  strong {
    font-weight: ${({ theme: t }) => t?.typography?.fontWeightBold};
  }
   

  .c-flex { display: flex; }
  .c-flex-inline { display: inline-flex; }
  .c-flex-row { flex-direction: row; }
  .c-flex-column { flex-direction: column; }
  .c-flex-grow-0 { flex-grow: 0; }
  .c-flex-grow-1 { flex-grow: 1; }
  .c-flex-shrink-0 { flex-shrink: 0; }
  .c-flex-shrink-1 { flex-shrink: 1; }
  .c-align-start { align-items: flex-start; }
  .c-align-center { align-items: center; }
  .c-align-end { align-items: flex-end; }
  .c-justify-start { justify-content: flex-start; }
  .c-justify-center { justify-content: center; }
  .c-justify-end { justify-content: flex-end; }
  .c-justify-space { justify-content: space-between; }
  
`;

export default GlobalStyle;
