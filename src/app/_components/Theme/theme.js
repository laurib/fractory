const defaultTheme = {
  palette: {
    body: '#fafafa',
    main: '#333f48',
    light: '#979ea2',
    blue: '#0072ce',
    red: '#f9423a',
    green: '#18ad18',
  },
  typography: {
    fontFamilyMain: '"Montserrat",sans-serif',
    fontSize: '1rem',
    fontSizeSmall: '0.75rem',
    fontSizeHtml: '16px',
    fontWeightBold: '600',
    fontWeightNormal: '400',
    lineHeight: '1.3',
  },
};

export default defaultTheme;
