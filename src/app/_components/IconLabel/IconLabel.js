import React from 'react';
import PropTypes from 'prop-types';

const IconLabel = ({ children, icon: Icon, iconSize, viewBox }) => (
  <div className="c-flex-inline c-align-center">
    <Icon size={iconSize} viewBox={viewBox} style={{ marginRight: '5px' }} />
    {children}
  </div>
);

IconLabel.defaultProps = {
  iconSize: undefined,
  viewBox: undefined,
};
IconLabel.propTypes = {
  icon: PropTypes.elementType.isRequired,
  iconSize: PropTypes.string,
  viewBox: PropTypes.string,
};

export default React.memo(IconLabel);
