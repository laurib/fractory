import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Element = styled.button.attrs(() => ({ type: 'button', tabIndex: 0 }))`
  -webkit-tap-highlight-color: transparent;
  align-items: center;
  appearance: none;
  background-color: transparent;
  border: 0;
  border-radius: 0;
  color: ${({ theme: t, variant }) => (variant ? t?.palette[variant] : 'inherit')};
  cursor: pointer;
  display: inline-flex;
  font: inherit;
  line-height: inherit;
  margin: 0 2px;
  justify-content: center;
  outline: none;
  padding: 0;
  position: relative;
  text-decoration: none;
  user-select: none;

  ::-moz-focus-inner {
    border: none;
    padding: 0;
  }
`;

const IconButton = ({ children, icon: Icon, iconSize, viewBox, ...other }) => (
  <Element {...other}>
    <Icon size={iconSize} viewBox={viewBox} />
    {children}
  </Element>
);

IconButton.defaultProps = {
  iconSize: '16px',
  viewBox: undefined,
};
IconButton.propTypes = {
  icon: PropTypes.elementType.isRequired,
  iconSize: PropTypes.string,
  viewBox: PropTypes.string,
};

export default React.memo(IconButton);
