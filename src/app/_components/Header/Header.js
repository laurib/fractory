import React from 'react';
import styled from 'styled-components';
import { useAuth } from 'Auth';

const Element = styled.div`
  height: 60px;
  padding: 0 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid ${({ theme: t }) => t.palette?.light};
`;

const Header = ({ children, ...other }) => {
  const auth = useAuth();
  const handleSignOut = () => {
    auth.signOut();
  };
  return (
    <Element {...other}>
      <h1>{children}</h1>
      {auth?.user && (
        <button type="button" onClick={handleSignOut}>
          SignOut
        </button>
      )}
    </Element>
  );
};
export default Header;
