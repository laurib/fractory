import { useEffect, useState } from 'react';
import api, { CancelToken } from 'api';
import moment from 'moment';
import getSession from './getSession';

const KEY = 'session';

const useProvideAuth = () => {
  const source = CancelToken.source();
  const session = getSession();

  const [state, setState] = useState(
    session
      ? {
          token: session.token,
          user: session.user,
        }
      : {}
  );

  const handleStorageEvent = () => {
    const s = getSession();
    setState({
      token: s.token,
      user: s.user,
    });
  };

  const signOut = () => {
    localStorage.removeItem(KEY);
    setState({});
  };

  const signIn = ({ email, password }) => {
    const promise = api.post('/login', { email, password }, { cancelToken: source.token });

    promise.then(({ data: { data: { id, token } } }) => {
      const obj = {};
      if (token && id) {
        obj.user = { id };
        obj.token = token;

        localStorage.setItem(
          KEY,
          JSON.stringify({
            ...obj,
            expire: moment()
              .add(60, 'm')
              .format(),
          })
        );

        setState(obj);
      } else {
        signOut();
      }
    });
    return promise;
  };

  useEffect(() => {
    if (window.addEventListener) {
      window.addEventListener('storage', handleStorageEvent, false);
    } else if (window.attachEvent) {
      window.attachEvent('storage', handleStorageEvent);
    }
    return () => {
      if (window.removeEventListener) {
        window.removeEventListener('storage', handleStorageEvent);
      } else if (window.detachEvent) {
        window.detachEvent('storage', handleStorageEvent);
      }
    };
  });

  useEffect(() => {
    return () => {
      source.cancel();
    };
  }, [source]);

  return {
    ...state,
    signIn,
    signOut,
  };
};

export default useProvideAuth;
