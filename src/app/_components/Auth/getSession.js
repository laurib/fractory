import moment from 'moment';

const KEY = 'session';

export default function() {
  const o = JSON.parse(localStorage.getItem(KEY)) || null;
  if (o?.token && o?.user?.id && o?.expire && moment(o.expire) > moment()) {
    return o;
  }
  return false;
}
