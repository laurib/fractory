import React, { useEffect, useState, useRef } from 'react';
import Header from 'Header';
import { useAuth } from 'Auth';

const LoginView = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const auth = useAuth();
  const isMounted = useRef(true);
  const emailRef = useRef(null);
  const passwordRef = useRef(null);

  const handleSignIn = () => {
    setLoading(true);
    setError(true);
    const {
      current: { value: email },
    } = emailRef;
    const {
      current: { value: password },
    } = passwordRef;

    auth
      .signIn({ email, password })
      .finally(() => {
        if (isMounted.current) {
          setLoading(false);
          setError(false);
        }
      })
      .catch(e => {
        if (isMounted.current) {
          setError(e.response.statusText);
        }
      });
  };

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  return (
    <>
      <Header>Login</Header>

      <div style={{ padding: '20px' }}>
        <div>
          <input type="text" ref={emailRef} defaultValue="testadmin@example.net" />
        </div>
        <div>
          <input type="password" ref={passwordRef} defaultValue="password123" />
        </div>
        <div>
          <button type="button" onClick={handleSignIn}>
            Sign in {loading && <span>(Loading)</span>}
          </button>
          {error && <div style={{ paddingTop: '10px' }}>{error}</div>}
        </div>
      </div>
    </>
  );
};

export default React.memo(LoginView);
