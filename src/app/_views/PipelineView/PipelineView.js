import React from 'react';
import Header from 'Header';
import List from 'PipelineList';

const PipelineView = () => {
  return (
    <>
      <Header>Pipeline</Header>
      <List />
    </>
  );
};

export default React.memo(PipelineView);
