import React from 'react';
import produce from 'immer';

export const Context = React.createContext();

/* eslint-disable no-param-reassign */
export default produce((state, { type, payload }) => {
  switch (type) {
    case 'SET_DATA':
      return payload;
    case 'REMOVE_ITEM':
      state.splice(state.findIndex(order => order.id === payload), 1);
      return state;
    case 'CHANGE_STATUS':
      state[state.findIndex(order => order.id === payload.id)].status = payload.status;
      return state;
    default:
      throw new Error();
  }
});
/* eslint-enable no-param-reassign */
