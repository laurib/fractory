import React, { useState, useEffect, useReducer, lazy, Suspense } from 'react';
import api, { CancelToken } from 'api';
import styled from 'styled-components';
import { getSession } from 'Auth';

import reducer, { Context } from 'PipelineDucks';

const Content = lazy(() => import('PipelineContent'));

const Element = styled.div`
  padding: 20px;
`;

const PipelineList = props => {
  const [state, dispatch] = useReducer(reducer, []);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    let isMounted = true;

    const {
      user: { id },
    } = getSession();

    const source = CancelToken.source();

    api
      .get('/pipeline', {
        params: { sales_user_id: id },
        cancelToken: source.token,
      })
      .then(({ data: { data: response } }) => {
        if (isMounted) {
          setLoading(false);
          dispatch({ type: 'SET_DATA', payload: response });
        }
      })
      .catch(err => {
        if (isMounted) {
          setError(err);
        }
      });

    return () => {
      isMounted = false;
      source.cancel();
    };
  }, []);

  if (error) {
    return <Element>Error</Element>;
  }
  if (loading) {
    return <Element>Loading...</Element>;
  }

  return (
    <Suspense fallback={<></>}>
      <Element {...props}>
        <Context.Provider value={dispatch}>
          <Content data={state} />
        </Context.Provider>
      </Element>
    </Suspense>
  );
};

export default React.memo(PipelineList);
