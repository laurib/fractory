import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import _ from 'lodash';
import Group from 'Group';

const Element = styled.div`
  display: flex;
  margin: 0 -10px;
`;

const Content = ({ data, ...other }) => {
  const groups = [
    'QUOTE_PENDING',
    'QUOTE_READY',
    'QUOTE_ACCEPTED',
    'READY_FOR_PRODUCTION',
    'IN_PRODUCTION',
    'SHIPPED',
  ];
  const groupedData = _.groupBy(data, 'status');

  return (
    <Element {...other}>
      {groups.map(group => {
        return (
          <Group key={group} status={group} data={groupedData[group]} groupsCount={groups.length} />
        );
      })}
    </Element>
  );
};

Content.defaultProps = {
  data: [],
};

Content.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
};

export default React.memo(Content);
