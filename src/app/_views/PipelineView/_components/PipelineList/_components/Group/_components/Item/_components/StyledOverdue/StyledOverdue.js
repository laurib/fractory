import React from 'react';
import styled from 'styled-components';

const Element = styled.div`
  color: ${({ theme: t }) => t?.palette?.red};
  text-transform: uppercase;
  text-align: right;
  font-size: 10px;
`;

export default React.memo(Element);
