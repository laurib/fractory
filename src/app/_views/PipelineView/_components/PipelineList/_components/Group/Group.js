import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import _ from 'lodash';
import GroupHeader from 'GroupHeader';
import Item from 'Item';

const Element = styled.div`
  box-sizing: border-box;
  flex-grow: 0;
  flex-shrink: 0;
  width: ${({ groupsCount }) => 100 / groupsCount}%;

  > div {
    margin: 0 10px;
  }
`;

const Group = ({ data, groupsCount, status, ...other }) => {
  const currencyTaxedTotalGroups = _.transform(
    _.transform(
      data,
      (result, value) => {
        const taxed = value.taxed_total || 0;
        if (result[value.currency]) {
          // eslint-disable-next-line no-param-reassign
          result[value.currency] += taxed;
        } else {
          // eslint-disable-next-line no-param-reassign
          result[value.currency] = taxed;
        }
      },
      {}
    ),
    (result, value, key) => {
      // eslint-disable-next-line no-param-reassign
      result[key] = Math.round(value);
    }
  );

  return (
    <Element groupsCount={groupsCount} {...other}>
      <div>
        <GroupHeader
          status={status}
          count={data.length}
          totalEur={currencyTaxedTotalGroups.EUR}
          totalGbp={currencyTaxedTotalGroups.GBP}
        />
        {data.map(item => (
          <Item key={item.id} data={item} />
        ))}
      </div>
    </Element>
  );
};

Group.defaultProps = {
  data: [],
  groupsCount: 1,
};

Group.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  groupsCount: PropTypes.number,
  status: PropTypes.string.isRequired,
};

export default React.memo(Group);
