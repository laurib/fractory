import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import { IconEuro } from 'Icon';
import IconLabel from 'IconLabel';

const color = ({ theme: t, dueStatus: status }) => {
  if (status === 1) {
    return t?.palette?.green;
  }
  if (status === 2) {
    return t?.palette?.red;
  }
  return t?.palette?.light;
};
const Element = styled.div`
  color: ${color};
`;

const DueStatus = ({ status, due, className: classNameProp, ...other }) => {
  const className = classNames('c-flex c-align-center', classNameProp);
  return due ? (
    <Element dueStatus={status} className={className} {...other}>
      <IconLabel icon={IconEuro} iconSize="16px">
        {due}
      </IconLabel>
    </Element>
  ) : null;
};

DueStatus.defaultProps = {
  className: '',
  status: 0,
  due: '',
};

DueStatus.propTypes = {
  className: PropTypes.string,
  status: PropTypes.number,
  due: PropTypes.string,
};

export default React.memo(DueStatus);
