import React from 'react';
import PropTypes from 'prop-types';
import pluralize from 'pluralize';
import Element from 'StyledOverdue';

const Overdue = ({ days, ...other }) => (
  <Element {...other}>Payment overdue {pluralize('day', days, true)}</Element>
);

Overdue.propTypes = {
  days: PropTypes.number.isRequired,
};

export default React.memo(Overdue);
