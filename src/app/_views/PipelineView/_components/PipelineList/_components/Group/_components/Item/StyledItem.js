import React from 'react';
import styled from 'styled-components';
import Paper from 'Paper';

const borderLine = ({ theme: t, dueStatus }) => {
  if (dueStatus === 1) {
    return t?.palette?.green;
  }
  if (dueStatus === 2) {
    return t?.palette?.red;
  }

  return 'none';
};

const StyledItem = styled(Paper)`
  :before {
    display: block;
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 2px;
    background: ${borderLine};
  }
`;

export default React.memo(StyledItem);
