import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Paper from 'Paper';
import Header from 'ItemHeader';
import pluralize from 'pluralize';

const Element = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  color: ${({ theme: t }) => t?.palette?.light};

  > div {
    padding: 5px;
    &:first-child {
      padding-left: 0;
    }
    &:last-child {
      padding-right: 0;
    }
  }
`;

const StyledPaper = styled(Paper)`
  border-bottom: 1px solid ${({ theme: t }) => t?.palette?.main};
`;

const GroupHeader = ({ count, totalEur, totalGbp, status, ...other }) => {
  const lang = {
    QUOTE_PENDING: 'RFQ',
    QUOTE_READY: 'Quote ready',
    QUOTE_ACCEPTED: 'Quote accepted',
    READY_FOR_PRODUCTION: 'Available for production',
    IN_PRODUCTION: 'In production',
    SHIPPED: 'Shipped',
  };

  const title = lang[status] || status;

  return (
    <StyledPaper {...other}>
      <Header title={title} />
      <Element>
        <div>{pluralize('deal', count, true)}</div>
        <div>{totalEur} €</div>
        <div>{totalGbp} £</div>
      </Element>
    </StyledPaper>
  );
};

GroupHeader.defaultProps = {
  count: 0,
  totalEur: 0,
  totalGbp: 0,
};

GroupHeader.propTypes = {
  count: PropTypes.number,
  status: PropTypes.string.isRequired,
  totalEur: PropTypes.number,
  totalGbp: PropTypes.number,
};

export default React.memo(GroupHeader);
