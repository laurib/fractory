import React from 'react';
import PropTypes from 'prop-types';
import Element from 'StyledOverdue';
import pluralize from 'pluralize';

const Overdue = ({ days, ...other }) => (
  <Element {...other}>Should have shipped {pluralize('day', days, true)} ago</Element>
);

Overdue.propTypes = {
  days: PropTypes.number.isRequired,
};

export default React.memo(Overdue);
