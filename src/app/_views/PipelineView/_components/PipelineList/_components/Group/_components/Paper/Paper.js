import React from 'react';
import styled from 'styled-components';

const Paper = styled.div`
  min-height: 60px;
  padding: 5px 10px;
  box-sizing: border-box;
  position: relative;
  display: flex;
  flex-direction: column;
  box-shadow: 0 0 1px rgba(0, 0, 0, 0.4);
  font-size: ${({ theme: t }) => t?.typography?.fontSizeSmall};
  margin: 10px 0;
  background: white;

  .text-light {
    color: ${({ theme: t }) => t?.palette?.light};
  }
`;

export default React.memo(Paper);
