import React, { useContext, useCallback } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Header from 'ItemHeader';
import ShippingStatus from 'ShippingStatus';
import DueStatus from 'DueStatus';
import UpdateStatus from 'UpdateStatus';
import Options from 'OptionsButton';
import PaymentOverdue from 'PaymentOverdue';
import ShippingOverdue from 'ShippingOverdue';
import Description from 'Description';
import ActionButtons from 'ActionButtons';
import Info from 'Info';
import { IconAccept, IconRefuse, IconForward, IconComplete } from 'Icon';
import IconButton from 'IconButton';
import { Context } from 'PipelineDucks';
import StyledItem from './StyledItem';

const invoiceDueStatus = ({ invoiceDue, paidAmount = 0, taxedTotal = 0 }) => {
  if (!invoiceDue) {
    return 0;
  }

  if (paidAmount >= taxedTotal) {
    return 1;
  }

  const due = moment(invoiceDue, 'DD.MM.YYYY').endOf('day');
  if (due < moment().startOf('day')) {
    return 2;
  }

  return 0;
};

const Item = ({ data: order, ...other }) => {
  const {
    available,
    client,
    currency,
    days_overdue: daysOverdue,
    days_since_production_ready_at: daysSinceProductionReady,
    declined,
    hours_since_updated: hoursSinceUpdated,
    id,
    invoice_due: invoiceDue,
    manufacturer,
    paid_amount: paidAmount,
    taxed_total: taxedTotal,
    production_ready_at: productionReady,
    shipping_pickup_at: shippingPickup,
    status,
    updated_at: updatedAt,
  } = order;

  const dispatch = useContext(Context);
  const dueStatus = invoiceDueStatus({ invoiceDue, paidAmount, taxedTotal });

  const handleItemRemove = useCallback(() => {
    dispatch({ type: 'REMOVE_ITEM', payload: id });
  }, [dispatch, id]);

  const handleStatusChange = useCallback(
    newStatus => () => {
      dispatch({ type: 'CHANGE_STATUS', payload: { id, status: newStatus } });
    },
    [dispatch, id]
  );

  return (
    <StyledItem dueStatus={dueStatus} {...other}>
      <Header title={id}>
        {status === 'QUOTE_READY' && <UpdateStatus updatedAt={updatedAt} />}
        {status === 'QUOTE_ACCEPTED' && <DueStatus status={dueStatus} due={invoiceDue} />}
        {status === 'READY_FOR_PRODUCTION' && <UpdateStatus hours={hoursSinceUpdated} />}
        {status === 'IN_PRODUCTION' && (
          <ShippingStatus readyAt={productionReady} pickup={!!shippingPickup} />
        )}
        <Options />
      </Header>
      <div className="c-flex c-justify-space" style={{ paddingBottom: '5px' }}>
        <Description
          manufacturer={manufacturer?.name}
          client={client}
          taxedTotal={taxedTotal}
          currency={currency}
        />
        {(!!available.length || !!declined.length) && (
          <Info available={available} declined={declined} />
        )}
        <ActionButtons>
          {status === 'QUOTE_READY' && (
            <>
              <IconButton icon={IconRefuse} variant="blue" onClick={handleItemRemove} />
              <IconButton
                icon={IconAccept}
                variant="blue"
                onClick={handleStatusChange('QUOTE_ACCEPTED')}
              />
            </>
          )}
          {status === 'READY_FOR_PRODUCTION' && (
            <IconButton
              icon={IconForward}
              variant="blue"
              onClick={handleStatusChange('IN_PRODUCTION')}
            />
          )}
          {status === 'IN_PRODUCTION' && (
            <IconButton icon={IconForward} variant="blue" onClick={handleStatusChange('SHIPPED')} />
          )}
          {status === 'SHIPPED' && (
            <IconButton icon={IconComplete} variant="blue" onClick={handleItemRemove} />
          )}
        </ActionButtons>
      </div>
      {dueStatus === 2 && !!daysOverdue && <PaymentOverdue days={daysOverdue} />}
      {status === 'IN_PRODUCTION' && !!daysSinceProductionReady && (
        <ShippingOverdue days={daysSinceProductionReady} />
      )}
    </StyledItem>
  );
};
Item.defaultProps = {
  data: [],
};

Item.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
  }),
};

export default React.memo(Item);
