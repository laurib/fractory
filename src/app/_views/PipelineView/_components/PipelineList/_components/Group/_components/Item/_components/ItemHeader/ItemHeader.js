import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Element = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 5px 0;

  h2 {
    color: ${({ theme: t }) => t?.palette?.main};
  }
`;

const ItemHeader = ({ title, children, ...other }) => (
  <Element {...other}>
    <div className="c-flex-grow-1">
      <h2>{title}</h2>
    </div>
    {children && <div className="c-flex c-flex-shrink-0 c-align-center">{children}</div>}
  </Element>
);

ItemHeader.defaultProps = {
  title: '',
};

ItemHeader.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default React.memo(ItemHeader);
