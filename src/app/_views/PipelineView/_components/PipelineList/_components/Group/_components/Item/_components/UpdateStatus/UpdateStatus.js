import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import { IconInvoice } from 'Icon';
import IconLabel from 'IconLabel';

const UpdateStatus = ({ updatedAt, hours, className: classNameProp, ...other }) => {
  const className = classNames('text-light c-flex c-align-center', classNameProp);
  const date = updatedAt ? moment(updatedAt, 'DD.MM.YYYY HH:mm:ss').format('DD.MM.YYYY') : '';

  return hours ? (
    <div className={classNameProp} {...other}>
      {hours}h
    </div>
  ) : (
    <div className={className} {...other}>
      <IconLabel icon={IconInvoice} iconSize="16px">
        {date}
      </IconLabel>
    </div>
  );
};

UpdateStatus.defaultProps = {
  className: '',
  hours: undefined,
  updatedAt: undefined,
};

UpdateStatus.propTypes = {
  className: PropTypes.string,
  updatedAt: PropTypes.string,
  hours: PropTypes.number,
};

export default React.memo(UpdateStatus);
