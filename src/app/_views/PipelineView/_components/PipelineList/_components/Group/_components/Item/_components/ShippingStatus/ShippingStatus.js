import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styled from 'styled-components';
import { IconDelivery } from 'Icon';
import IconLabel from 'IconLabel';

const Element = styled.div`
  color: ${({ theme: t, pickup }) => (pickup ? t?.palette?.green : t?.palette?.light)};
`;

const ShippingStatus = ({ readyAt, pickup, className: classNameProp, ...other }) => {
  const className = classNames('c-flex c-align-center', classNameProp);
  return (
    <Element pickup={pickup} className={className} {...other}>
      <IconLabel icon={IconDelivery} iconSize="16px">
        {readyAt}
      </IconLabel>
    </Element>
  );
};

ShippingStatus.defaultProps = {
  className: '',
  readyAt: '',
  pickup: false,
};

ShippingStatus.propTypes = {
  className: PropTypes.string,
  readyAt: PropTypes.string,
  pickup: PropTypes.bool,
};

export default React.memo(ShippingStatus);
