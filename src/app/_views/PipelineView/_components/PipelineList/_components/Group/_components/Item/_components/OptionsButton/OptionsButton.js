import React from 'react';
import styled from 'styled-components';

const Element = styled.div`
  position: relative;
  height: 14px;
  width: 10px;
  margin-left: 10px;

  :before,
  :after {
    display: block;
    content: '';
  }
  :before,
  :after,
  > div {
    height: 2px;
    width: 2px;
    position: absolute;
    border-radius: 2px;
    background: ${({ theme: t }) => t?.palette?.light};
    top: 0;
    left: 50%;
    margin-left: -1px;
  }
  div {
    top: 2px;
  }
  :before {
    top: 6px;
  }

  &:after {
    top: 10px;
  }
`;

const OptionsButton = props => (
  <Element {...props}>
    <div />
  </Element>
);
export default React.memo(OptionsButton);
