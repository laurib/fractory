import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Element = styled.div`
  flex-shrink: 0;
  padding-right: 10px;
`;

const Description = ({ client, currency, manufacturer, taxedTotal, ...other }) => (
  <Element {...other}>
    <div className="text-light">{manufacturer}</div>
    <div className="text-light">{client}</div>
    {taxedTotal && (
      <div>
        {taxedTotal} {currency}
      </div>
    )}
  </Element>
);

Description.defaultProps = {
  client: '-',
  currency: null,
  manufacturer: '-',
  taxedTotal: null,
};
Description.propTypes = {
  client: PropTypes.string,
  currency: PropTypes.string,
  manufacturer: PropTypes.string,
  taxedTotal: PropTypes.number,
};

export default React.memo(Description);
