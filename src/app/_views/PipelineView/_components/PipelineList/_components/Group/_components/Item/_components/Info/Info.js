import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Element = styled.div`
  flex-shrink: 1;
  padding: 0 5px;

  > .line-through {
    text-decoration: line-through;
  }
`;

const Info = ({ available, declined, ...other }) => (
  <Element {...other}>
    {available.map((o, i) => o.name && <div key={o.id || i}>{o.name}</div>)}
    {declined.map(
      (o, i) =>
        o.name && (
          <div key={o.id || i} className="line-through">
            {o.name}
          </div>
        )
    )}
  </Element>
);

Info.defaultProps = {
  available: [],
  declined: [],
};
Info.propTypes = {
  available: PropTypes.arrayOf(PropTypes.object),
  declined: PropTypes.arrayOf(PropTypes.object),
};

export default React.memo(Info);
