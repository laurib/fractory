import React from 'react';
import styled from 'styled-components';

const ActionButtons = styled.div`
  flex-shrink: 0;
  display: flex;
  align-items: flex-end;
  padding-left: 10px;
`;

export default React.memo(ActionButtons);
