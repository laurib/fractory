import axios, { CancelToken } from 'axios';
import { getSession } from 'Auth';

const API_URL = 'https://staging.fractory.com/api/v1';

const instance = axios.create({
  baseURL: API_URL,
});

instance.interceptors.request.use(
  config => {
    const { token } = getSession();
    if (token) {
      // eslint-disable-next-line
      config.headers.common.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);
export default instance;
export { CancelToken };
