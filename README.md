### Setting up environment

- run `yarn install`

### Development

- run `yarn run start`

### Build

- run `yarn run build`

### Serving build

After build

- run `yarn run live`
